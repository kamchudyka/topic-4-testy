package com.inzynierka.controllers;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@Transactional
public class ExampleIntegrationTest extends WebAppIT {

    //Wstrzyknij ziarenko TransferController;

    //Wstrzyknij ziarenko RoleRepository;

    //Wstrzyknij ziarenko UserService;

    //Wstrzyknij ziarenko UserRepository;

    //Zdefiniuj obiekt principal jako obiekt 'zamockowany'

    //Zdefiniuj obiekty dwoch uzytkownikow: sender oraz receiver


    //Metoda before wykonuje się przed każdym testem. Wykorzystaj ją do:
    //Zrobienia 'stuba' dla naszego 'zamockowanego' obiektu principal. Stub powinien wygladać tak: 'kiedy principal.getName() to zwróć nazwę użytkownika sender'.

    //Zbuduj uzytkownikow sender oraz receiver. Wykorzystaj do tego metodę buildMockUser(), która została użyta w innych klasach testowych. Rozszerz tą metodę o 2 parametry:
    //email oraz userName po to aby nie utworzyć dwa razy użytkownika o tej samej nazwie czy adresie email.

    //Użyj metod z serwisu bądź repozytorium aby zapisać wcześniej utworzonych użytkowników.

    @Before
    public void setUp() {
    }

    @Test
    public void shouldTransferMoneyToAnotherUser() throws Exception {
        //given
        //Tutaj nie musimy nic wpisywać, ponieważ już zdefiniowaliśmy nasze obiekty wstępne w metodzie setUp().

        //when
        //Zgodnie z konwencją GWT w tym miejscu wykonujemy właściwą operację, którą chcemy przetestować. W tym przypadku będzie to wysłanie żądania do serwera o przelanie pieniędzy z
        //jednego użytkownika do drugiego. Pamiętaj o tym, aby zmienić przekazywany obiekt principal z null na nasz 'zamockowany'.

        //then
        //Tutaj zawsze odbywa się sprawdzenie czy nasza operacja wykonana w klauzuli 'when' powiodła się. W tym przypadku musimy pobrać naszego receiver'a z bazy danych, a następnie
        //sprawdzić czy kwota na jego koncie głównym wynosi tyle, ile przelaliśmy mu pieniędzy.
        //Hint: Wykorzystaj metodę assertTrue (z biblioteki Junit), bądź assertThat (z biblioteki hamcrest). Obie opcje są poprawne.

    }

    private void userTriesTo(String userName, String accuntType, int amount) throws Exception {
        MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();

        multiValueMap.add("recipientName", userName);
        multiValueMap.add("accountType", accuntType);
        multiValueMap.add("amount", Integer.toString(amount));


        mvc.perform(post("/transfer/toSomeoneElse")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .principal(null)
                .params(multiValueMap))
                .andDo(print());
    }

}
