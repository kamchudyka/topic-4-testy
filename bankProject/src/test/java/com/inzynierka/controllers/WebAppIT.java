package com.inzynierka.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.inzynierka.InzynierkaApplication;
import com.inzynierka.InzynierkaApplicationTests;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import java.util.Map;
import java.util.Set;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

/**
 * Created by JK82FU on 2017-11-19.
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { InzynierkaApplication.class})
@AutoConfigureMockMvc
public abstract class WebAppIT extends InzynierkaApplicationTests {

    @Autowired
    protected WebApplicationContext wac;

    @Autowired
    ObjectMapper objectMapper;

    protected MockMvc mvc;

    protected MockMvc mvcWithAuthentication;

    protected MockHttpSession session;

    @Before
    public void setupAbstractTest() {
        this.mvc = buildMockMvc();
        this.mvcWithAuthentication = buildMockMvcWithUserAuthentication();
        this.session = buildMockSession();
    }

    protected MockHttpSession buildMockSession(){
        return new MockHttpSession(wac.getServletContext());
    }

    protected MockMvc buildMockMvc() {
        return MockMvcBuilders
                .webAppContextSetup(wac)
                .build();
    }

    protected MockMvc buildMockMvcWithUserAuthentication() {
        return MockMvcBuilders.webAppContextSetup(wac)
                .apply(springSecurity())
                .build();
    }


    protected MultiValueMap<String, String> toFormParams(Object o, Set<String> excludeFields) throws Exception {
        ObjectReader reader = objectMapper.readerFor(Map.class);
        Map<String, String> map = reader.readValue(objectMapper.writeValueAsString(o));

        MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();
        map.entrySet().stream()
                .filter(e -> !excludeFields.contains(e.getKey()))
                .forEach(e -> multiValueMap.add(e.getKey(), (e.getValue() == null ? "" : e.getValue())));
        return multiValueMap;
    }
}
