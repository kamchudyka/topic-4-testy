package com.inzynierka.services;

import com.inzynierka.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(MockitoJUnitRunner.class)
public class ExampleUnitTest {

    private static final String USER_NAME = "testUser";

    //Utworz imitacje interfejsu UserRepository - skorzystaj z adnotacji @Mock

    //Utworz imitacje klasy User

    //Zadeklaruj zmienna typu UserSecurityService


    @Before
    public void setUp(){
        //Zainicjuj zmienna userSercuritService uzywajac domyslnego konstruktora tej klasy

        ReflectionTestUtils.setField(null, null, null, UserRepository.class);

        //zdefiniuj stub o treści: kiedy userRepository.findByUserName(dowolny obiekt) to zwróć imitację obiektu User
        //zdefiniuj stub o treści: kiedy user.getUsername() to zwróć stałą USER_NAME zdefiniowaną na początku klasy
    }

    @Test
    public void shouldFindCreatedUser(){
        //given

        //when
        //Testy jednostkowe sprawdzają pojedyńcze komponenty w kodzie. Przykładem może być metoda loadUserByUsername z seriwsu UserSecurityService.
        //Wywołajmy tą metodę oraz przekażmy do niej nazwę wcześniej utworzonej przez nas imitacji użytkownika. Przypiszmy jej wynik do zmiennej.

        //then
        //Na końcu sprawdźmy czy zwrócony użytkownik to jest ten sam, który przekazaliśmy do metody z serwisu UserSecurityService

    }

}
