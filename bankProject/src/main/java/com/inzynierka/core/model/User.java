package com.inzynierka.core.model;

import com.inzynierka.core.model.security.Authority;
import com.inzynierka.core.model.security.UserRole;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Entity
public class User implements UserDetails{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long userId;

    private String username;

    private String password;

    private String firstName;

    private String lastName;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    private String phone;

    private boolean enabled=true;

    @OneToOne
    private PrimaryAccount primaryAccount;

    @OneToOne
    private SavingsAccount savingsAccount;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<UserRole> userRoles = new HashSet<>();

    public Set<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Set<UserRole> userRoles) {
        this.userRoles = userRoles;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public PrimaryAccount getPrimaryAccount() {
        return primaryAccount;
    }

    public void setPrimaryAccount(PrimaryAccount primaryAccount) {
        this.primaryAccount = primaryAccount;
    }

    public SavingsAccount getSavingsAccount() {
        return savingsAccount;
    }

    public void setSavingsAccount(SavingsAccount savingsAccount) {
        this.savingsAccount = savingsAccount;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", userRoles=" + userRoles +
                '}';
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> authorities = new HashSet<>();
        userRoles.forEach(ur -> authorities.add(new Authority(ur.getRole().getName())));
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public static class Builder {
        private User user;

        public Builder(){this.user = new User();}

        public Builder(User user){
            super();

            this.user = new User();
            this.user.setFirstName(user.getFirstName());
            this.user.setEnabled(user.isEnabled());
            this.user.setEmail(user.getEmail());
            this.user.setLastName(user.getLastName());
            this.user.setPassword(user.getPassword());
            this.user.setPhone(user.getPhone());
            this.user.setPrimaryAccount(user.getPrimaryAccount());
            this.user.setSavingsAccount(user.getSavingsAccount());
            this.user.setUsername(user.getUsername());
            this.user.setUserRoles(user.getUserRoles());
            this.user.setUserId(user.getUserId());
            this.user.setPhone(user.getPhone());
        }

        public Builder withFirstName(String firstName){
            this.user.setFirstName(firstName);

            return this;
        }

        public Builder withLastName(String lastName){
            this.user.setLastName(lastName);

            return this;
        }

        public Builder withEmail(String email){
            this.user.setEmail(email);

            return this;
        }

        public Builder isEnabled(boolean isEnabled){
            this.user.setEnabled(isEnabled);

            return this;
        }

        public Builder withPrimaryAccount(PrimaryAccount primaryAccount){
            this.user.primaryAccount = primaryAccount;

            return this;
        }

        public Builder withSavingsAccount(SavingsAccount savingsAccount){
            this.user.savingsAccount = savingsAccount;

            return this;
        }

        public Builder withUserName(String userName){
            this.user.setUsername(userName);

            return this;
        }

        public Builder withUserid(Long id){
            this.user.setUserId(id);

            return this;
        }

        public Builder withRoles(Set<UserRole> userRoles){
            this.user.setUserRoles(userRoles);

            return this;
        }

        public Builder withPassword(String password){
            this.user.password = password;

            return this;
        }

        public Builder withPhone(String phone){
            this.user.phone = phone;

            return this;
        }

        public User build(){
            return this.user;
        }
    }

}
