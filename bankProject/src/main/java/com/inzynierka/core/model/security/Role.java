package com.inzynierka.core.model.security;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
public class Role {
    @Id
    private int roleId;

    private String name;

    @OneToMany(mappedBy = "role", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<UserRole> userRoles = new HashSet<>();

    public Role() {

    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Set<UserRole> userRoles) {
        this.userRoles = userRoles;
    }


    public static final class RoleBuilder {

    private int roleId;
        private String name;
        private Set<UserRole> userRoles = new HashSet<>();

        public RoleBuilder() {
        }

        public static RoleBuilder aRole() {
            return new RoleBuilder();
        }

        public RoleBuilder withRoleId(int roleId) {
            this.roleId = roleId;
            return this;
        }

        public RoleBuilder withName(String name) {
            this.name = name;
            return this;
        }

        public RoleBuilder withUserRoles(Set<UserRole> userRoles) {
            this.userRoles = userRoles;
            return this;
        }

        public Role build() {
            Role role = new Role();
            role.setRoleId(roleId);
            role.setName(name);
            role.setUserRoles(userRoles);
            return role;
        }
    }
}
